#!/bin/sh
appName=$1
echo "paso1"
echo $HEROKU_API_KEY
imageId=$(docker inspect registry.heroku.com/excel-data-import/web --format={{.Id}})
payload='{"updates":[{"type":"web","docker_image":"'"$imageId"'"}]}'
curl -n -X PATCH https://api.heroku.com/apps/excel-data-import/formation \
-d "$payload" \
-H "Content-Type: application/json" \
-H "Accept: application/vnd.heroku+json; version=3.docker-releases" \
-H "Authorization: Bearer $HEROKU_API_KEY"
