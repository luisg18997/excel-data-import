FROM node

WORKDIR /api

COPY package*.json ./

RUN npm install

RUN npm install nodemon tsc ts-node typescript -g

COPY . .

EXPOSE 8080

CMD ["npm", "start"]
